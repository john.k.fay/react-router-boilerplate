import React, { Component } from "react"
import "./assets/css/App.css"
import { Header, Content } from "./components/"

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Content />
      </div>
    )
  }
}

export default App
