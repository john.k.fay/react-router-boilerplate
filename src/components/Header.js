import React, { Component } from "react"
import logo from "./../assets/images/logo.svg"
import { Link } from "react-router-dom"
import { Switch, Route } from "react-router-dom"
import { Home, Contact } from "./"

class Header extends Component {
  render() {
    return (
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <nav>
          <Link to="/">Home</Link>
          <Link to="/contact">Contact</Link>
        </nav>
        <Switch>
          <Route path="/" exact Component={Home} />
          <Route path="/contact" Component={Contact} />
        </Switch>
      </div>
    )
  }
}

export default Header
