import React, { Component } from "react"
import { Route } from "react-router-dom"
import { Home, Contact } from "./"

class Content extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/contact" component={Contact} />
      </div>
    )
  }
}

export default Content
